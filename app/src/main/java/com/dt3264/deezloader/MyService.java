package com.dt3264.deezloader;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.HandlerThread;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Objects;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.socket.client.IO;
import io.socket.client.Socket;

public class MyService extends Service {

    public static boolean isServiceRunning = false;
    /**
     * Rx2 components
     */
    public static Observer<Message> serviceDataHandler;
    static Socket socket;
    final int MAIN_NOTIFICATION_ID = 10;
    final String CHANNEL_ID = "com.dt3264.Deezloader";
    // NodeJs socket methods
    final String url = MainActivity.serverURL;
    int NOTIFICATION_ID = 100;
    Context context;
    SharedPreferences sharedPreferences;
    Thread serverThread;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            Objects.requireNonNull(intent.getAction());
            getString(R.string.serviceName);
        }
        return START_NOT_STICKY;
    }

    // In case the service is deleted or crashes some how
    @Override
    public void onDestroy() {
        stopMyService();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Used only in case of bound services.
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        HandlerThread thread = new HandlerThread("Thread Name");
        //Start the thread//
        thread.start();
        prepareHandler();
        createNotificationChannel();
        prepareNodeServerListeners();
        startService();
    }


    // Notification methods
    public void startService() {
        if (isServiceRunning) return;

        Bitmap serviceIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Intent serverNotifIntent = new Intent(getApplicationContext(), BrowserActivity.class)
                .setAction(Intent.ACTION_MAIN)
                .addCategory(Intent.CATEGORY_LAUNCHER)
                .addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent serverPendingIntent = PendingIntent.getActivity(this, 0, serverNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name))
                .setContentText(getResources().getString(R.string.notificationService))
                .setSmallIcon(R.mipmap.ic_notification)
                .setLargeIcon(Bitmap.createScaledBitmap(serviceIcon, 128, 128, false))
                .setOngoing(false)
                .setContentIntent(serverPendingIntent);

        startServer();
        startForeground(MAIN_NOTIFICATION_ID, notification.build());
    }

    void stopMyService() {
        if (serverThread != null)
            serverThread.interrupt();
        serverThread = null;
        stopForeground(true);
        stopSelf();
        isServiceRunning = false;
    }

    void startServer() {
        try {
            serverThread = new Thread(() -> {
                //The path where we expect the node project to be at runtime.
                String nodeDir = getApplicationContext().getFilesDir().getAbsolutePath() + "/deezerLoader";
                MainActivity.startNodeWithArguments(new String[]{"node", nodeDir + "/app.js"});
            });
            serverThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Service notifications";
            String description = "Channel when Deezloader server is running";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            channel.setSound(null, null);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            Objects.requireNonNull(notificationManager).createNotificationChannel(channel);
        }
    }

    void NotifyDownload(int progress, String songName) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext(), CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentTitle("Downloading: " + songName)
                .setSubText(progress > 0 ? progress + "%" : "")
                .setProgress(100, progress, (progress == 0))
                .setTimeoutAfter(60000)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mBuilder.setOnlyAlertOnce(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        // notificationId is a unique int for each notification that you must define
        if (progress < 100) notificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    void endNotification(String songName, songLastStatus lastStatus) {
        String message = "";
        switch (lastStatus) {
            case READY:
                message = "Downloaded: " + songName;
                break;
            case CANCELLED:
                message = "Cancelled: " + songName;
                break;

            case ALREADY_DOWNLOADED:
                message = "Already downloaded: " + songName;
                break;
        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getBaseContext(), CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_notification)
                .setContentTitle(message)
                .setPriority(NotificationCompat.PRIORITY_LOW);
        mBuilder.setOnlyAlertOnce(true);
        NotificationManagerCompat.from(this).notify(NOTIFICATION_ID, mBuilder.build());
        NOTIFICATION_ID++;
    }

    void prepareNodeServerListeners() {
        try {
            socket = IO.socket(url);
        } catch (URISyntaxException ignored) {
        }
        socket.on("siteReady", args -> {
            transmitMessage(new Message(1, getString(R.string.serverReady)));
            isServiceRunning = true;
        });
        socket.on("openLink", args -> {
            String url = (String) args[0];
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setData(Uri.parse(url));
            startActivity(i);
        });
        socket.on("progressData", args -> {
            Integer progress = 0;
            String songName;
            try {
                progress = (Integer) args[0];
            } catch (NullPointerException ignored) {
            }
            songName = (String) args[1];
            NotifyDownload(progress, songName);
        });
        socket.on("downloadCancelled", args -> {
            String songName = (String) args[0];
            endNotification(songName, songLastStatus.CANCELLED);
        });
        socket.on("downloadAlreadyExists", args -> {
            String songName = (String) args[0];
            endNotification(songName, songLastStatus.ALREADY_DOWNLOADED);
        });
        socket.on("downloadReady", args -> {
            String internalPath = (String) args[0];
            //String songName = (String) args[1];
            File internalFile;
            internalFile = new File(internalPath);
            scanNewSongInternal(Uri.fromFile(internalFile));
            //endNotification(songName, songLastStatus.READY);
        });
        socket.connect();
    }

    void prepareHandler() {
        serviceDataHandler = new Observer<Message>() {
            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(Message msg) {
                Log.d("asd", msg.getMessage());
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        };
    }

    void transmitMessage(Message msg) {
        if (MainActivity.mainDataHandler != null)
            Message.transmitMessage(msg)
                    // Run on a background thread
                    .subscribeOn(Schedulers.io())
                    // Be notified on the main thread
                    .observeOn(AndroidSchedulers.mainThread())
                    // The method that handles the data
                    .subscribe(MainActivity.mainDataHandler);
    }


    // File methods (and a class)
    void scanNewSongInternal(Uri fileUri) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(fileUri);
        sendBroadcast(intent);
    }

    void scanNewSongExternal(File externalFile) {
        new MediaScannerWrapper(context, externalFile.toString()).scan();
    }

    enum songLastStatus {
        READY,
        CANCELLED,
        ALREADY_DOWNLOADED
    }

    public class MediaScannerWrapper implements MediaScannerConnection.MediaScannerConnectionClient {
        private final MediaScannerConnection mConnection;
        private final String mPath;
        private final String mMimeType;

        // filePath - where to scan;
        // mime type of media to scan i.e. "image/jpeg".
        // use "*/*" for any media
        private MediaScannerWrapper(Context _ctx, String _filePath) {
            mPath = _filePath;
            mMimeType = "*/*";
            mConnection = new MediaScannerConnection(_ctx, this);
        }

        // do the scanning
        private void scan() {
            mConnection.connect();
        }

        // start the scan when scanner is ready
        public void onMediaScannerConnected() {
            mConnection.scanFile(mPath, mMimeType);
            Log.w("MediaScannerWrapper", "media file scanned: " + mPath);
        }

        public void onScanCompleted(String path, Uri uri) {
        }
    }
}
